﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;
using System.Xml.Linq;
using System.Text;

namespace SkyportAPI
{
    /// <summary>
    /// Summary description for SkyportAPIService
    /// </summary>
    [WebService(Namespace = "https://skydiscovery.com.au/", Name = "Skyport API Services", Description = "Skyport Web services")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class SkyportAPIService : System.Web.Services.WebService
    {
        [WebMethod]
        public bool CheckLicense(string licenseNumber, out string clientName, out string errorMessage)
        {
            bool validLicense = false;
            errorMessage = "";
            clientName = "";

            //load xml licenses file
            XDocument xmlLicenses = XDocument.Load(Server.MapPath("~/SkyportLicenses.xml"));

            var licenses = from l in xmlLicenses.Descendants("License")
                           select new
                           {
                               Key = l.Attribute("Key").Value.Trim(),
                               Client = l.Attribute("Client").Value.Trim(),
                               ExpiryDate = l.Attribute("ExpiryDate").Value.Trim()
                           };

            dynamic matchingLicense = null;
            foreach (var lic in licenses)
            {
                if (lic.Key.ToLower() == licenseNumber.Trim().ToLower())
                {
                    matchingLicense = lic;
                    break;
                }
            }

            if (matchingLicense == null)
            {
                errorMessage = "Invalid key";
            }
            else
            {
                DateTime dtExpiry = DateTime.ParseExact(matchingLicense.ExpiryDate, "dd-MMM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                if (dtExpiry.AddDays(1) < DateTime.Now)
                {
                    errorMessage = "License expired";
                }
                else
                {
                    clientName = matchingLicense.Client;
                    validLicense = true;
                    errorMessage = "";
                }
            }
            return validLicense;
        }

        [WebMethod]
        public void UpdateBillingInfo(string clientName, string userName, string workspaceName, string licenseNumber, string hostNameAndIPAddress,
            int numberOfDocuments, out string errorMessage)
        {
            errorMessage = "";
            StreamWriter sw = null;

            try
            {
                sw = new StreamWriter(System.Web.Configuration.WebConfigurationManager.AppSettings["BillingFilePath"].ToString(),
                true, Encoding.ASCII);

                if (sw == null)
                {
                    errorMessage = "Unable to update Billing info. File not found";
                }
                else
                {
                    sw.WriteLine(
                                    "\"" + DateTime.Now.ToString("dd-MMM-yyyy:hh:mm:ss tt") + "\"," +
                                    "\"" + clientName + "\"," +
                                    "\"" + userName + "\"," +
                                    "\"" + workspaceName + "\"," +
                                    "\"" + licenseNumber + "\"," +
                                    "\"" + hostNameAndIPAddress + "\"," +
                                    "\"" + numberOfDocuments + "\"");
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }
            finally
            {
                sw.Close();
            }
        }

        [WebMethod]
        public string GenerateKey()
        {
            return Guid.NewGuid().ToString();
        }

        [WebMethod()]
        public byte[] DownloadFile(string filePath)
        {
            System.IO.FileStream fStream = null;
            fStream = System.IO.File.Open(filePath, FileMode.Open, FileAccess.Read);
            byte[] bytes = new byte[fStream.Length];
            fStream.Read(bytes, 0, (int)fStream.Length);
            fStream.Close();
            return bytes;
        }
    }
}
